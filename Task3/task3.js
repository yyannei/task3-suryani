let reminders = [
]

function UpdateReminderContainer(){
    if (reminders.length === 0){
        document.getElementById('added-container').innerHTML = 
            "<h3 id='reminder-info-text'>Add a Reminder</h3>";
    }
    else {
        let remindersString = "";
        reminders.sort((a,b) => {
            if (a.schedule > b.schedule){
                return 1;
            }
            else if(a.schedule < b.schedule) {
                return -1;
            }
            else {
                return 0;
            }
        });
        
        reminders.forEach((reminder, index) => {
            let duration = moment(reminder.schedule).diff(moment(new Date()), 'seconds');
            if (duration < 0){
                duration = 0;
            }
            remindersString += `
                <div class="reminder reminder-pass" id="reminder${index}">
                    <div class="reminder-info">
                        <h3 class="reminder-info-title">${reminder.title}</h3>
                        <h5 class="reminder-info-schedule">${moment(reminder.schedule).format("DD MMMM YYYY, hh:mm A")}</h5>
                        <span class="counter">${duration} seconds</span></h5>
                    </div>
                    <div class="reminder-action">
                        <button onClick="DeleteReminder(${index})"><i class="far fa-trash-alt"></i></button>
                    </div>
                </div>
            `
        });
        document.getElementById('added-container').innerHTML = remindersString;
    }

}

function DeleteReminder(index){
    reminders.splice(index,1);
    UpdateReminderContainer();
}

function AddReminder() {
    let date = document.getElementById('form-date');
    let time = document.getElementById('form-time');
    let desc = document.getElementById('form-desc');
    let duration = moment(new Date(date.value + ' ' + time.value)).diff(moment(new Date()), 'seconds');
    if (!(date.value === "" || time.value === "" || desc.value == "")){
        reminders.push({
            title : desc.value,
            schedule : new Date(date.value + ' ' + time.value),
            timer : setTimeout(() => {
                let audio = document.getElementsByTagName('audio')[0];
                audio.play(); 
                myAlert();
            }, duration * 1000 )
        });
    
        UpdateReminderContainer();
    }
}

function myAlert() {
    let a = confirm("Your reminder is ringing! Press OK button to stop it.");
    if (a == true) {
        let audio = document.getElementsByTagName('audio')[0];
        audio.pause();
    }
    else {
        return 0;
    }
    document.getElementById("alert").innerHTML = txt;
}
  